# Transactions API

The transactions API allows to issue a transaction request which will create an entry in the database, issue a request to transfer the funds and store the transaction's ID and issue a request to update the wallet balance and store the transaction's ID.

You can access the "documentation" folder at the root of the repository to see diagrams detailing the inner workings of the API in the file "Transactions API Designs.pdf".
Note: This is a code showcase application, as such only one endpoint of the API has been implemented.

## Getting started

1. Download the source code.
2. In the "dependencies_docker_compose" you will find docker-compose files to create containers for Kafka and MongoDB.
Note: you can install the services in another location and provide your own connection string configurations, explained in step 6.
3. Create the Kafka container using "docker-compose up -d" from the "Kafka" folder.
4. Create the MongoDB container using "docker-compose up -d" from the "MongoDD" folder.
5. Open the code in your IDE of choice.
6. Open the "application-dev.yml" file and configure the MongoDB and Kafka connection URLs in case that you installed the services in a different location.
7. Run the application using your IDE.
8. SpringBoot's initialization logs should start rolling, if connection to kafka fails you should start seeing continuous logs about it.
9. Import the postman collection file "Transactions API.postman_collection.json" included in the "documentation" folder at the root of the repository.
10. Issue a request with the "Create Transaction" request under the "Transactions" folder.
11. If MongoDB has connection issues you should see an error here.
12. If everything went well, the response should include the data of the transaction and the entry should be stored in MongoDB.
13. By the time you test this the external API mock might be unavailable, in which case you can create a mock server using the postman collection examples included in the file "Mocks for Transaction API.postman_collection.json".

## Build and Containerize

1. Go to the root of the project where the "mvnw" file is located.
2. Execute the command "./mvnw install" to build the JAR file, it will be created in the "target" folder.
3. Execute the docker command "docker build --tag=transactions-api:latest ." to create a docker image
4. Execute the docker command "docker run -p8080:8080 transactions-api:latest -d" to run the container

Note: If you're running the dependencies from docker you will need to configure the container's network for the API container to be able to reach them if you haven't already.

