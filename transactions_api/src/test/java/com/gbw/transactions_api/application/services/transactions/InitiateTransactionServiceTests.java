package com.gbw.transactions_api.application.services.transactions;

import com.gbw.transactions_api.application.ports.input.transactions.requests.CreateTransactionRecipientRequest;
import com.gbw.transactions_api.application.ports.input.transactions.requests.CreateTransactionRequest;
import com.gbw.transactions_api.application.ports.output.IFindTransactionPort;
import com.gbw.transactions_api.domain.constants.TransactionEventsQueueNames;
import com.gbw.transactions_api.domain.entities.SourceAccountData;
import com.gbw.transactions_api.domain.entities.TransactionSettings;
import com.gbw.transactions_api.domain.entities.UserData;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@EmbeddedKafka(
        topics = {
                TransactionEventsQueueNames.PROCESS_PAYMENT_TOPIC},
        count = 1, partitions = 10,
        brokerProperties = { "listeners=PLAINTEXT://localhost:29092", "port=29092" })
@DirtiesContext
@SpringBootTest
public class InitiateTransactionServiceTests {
    @Autowired
    private EmbeddedKafkaBroker embeddedKafka;
    @Autowired
    private IFindTransactionPort findTransactionPort;
    @Autowired
    private InitiateTransactionService classToTest;
    private KafkaTemplate<Integer, String> kafkaTemplate;

    private int feePercentage = 10;

    public InitiateTransactionServiceTests() {
        classToTest = new InitiateTransactionService(
                null, null, new UserData("1000"),
                new TransactionSettings(10, 3, 15)
        );
    }

    @Test
    public void testCreateTransactionPaymentEvent() {
        var transactionValue = 27f;
        var request = new CreateTransactionRequest(
                transactionValue, new CreateTransactionRecipientRequest(
                        "TestName", "TestSurname", "100001",
                "20002", "30003", "TestBank"
        )
        );
        var response = classToTest.initiateTransaction(request);

        Assertions.assertNotNull(response);
        Assertions.assertNotNull(response.getId());
        Assertions.assertNotNull(response.getCreationDate());

        Assertions.assertEquals(transactionValue, response.getTransactionValue());
        Assertions.assertEquals(feePercentage, response.getFeePercentage());
        Assertions.assertEquals(transactionValue / feePercentage, response.getFeeValue());
        Assertions.assertEquals(transactionValue - (transactionValue / feePercentage), response.getTransactionMinusFeeValue());

        var createdTransaction = findTransactionPort.find(response.getId());
        Assertions.assertNotNull(createdTransaction);
    }
}
