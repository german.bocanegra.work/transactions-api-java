package com.gbw.transactions_api.adapter.output.events.kafka;

import com.gbw.transactions_api.domain.constants.TransactionEventsQueueNames;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.*;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;

@RunWith(SpringRunner.class)
@EmbeddedKafka(
        topics = {
                TransactionEventsQueueNames.PROCESS_WALLET_BALANCE_TOPIC},
        count = 1, partitions = 10,
        brokerProperties = { "listeners=PLAINTEXT://localhost:29092", "port=29092" })
@DirtiesContext
@SpringBootTest
public class CreateTransactionEventsKafkaServicePaymentTests {
    @Autowired
    private EmbeddedKafkaBroker embeddedKafka;
    private KafkaTemplate<Integer, String> kafkaTemplate;

    private CreateTransactionEventsKafkaService classToTest;

    public CreateTransactionEventsKafkaServicePaymentTests() {

    }

    private boolean called = false;

    @Before
    public void prepare() {
        if (called) {
            return;
        }
        Map<String, Object> producerProps= KafkaTestUtils.producerProps(embeddedKafka);
        ProducerFactory<Integer, String> producerFactory = new DefaultKafkaProducerFactory<>(producerProps);
        kafkaTemplate = new KafkaTemplate<>(producerFactory);

        classToTest = new CreateTransactionEventsKafkaService(kafkaTemplate);

        called = true;
    }

    @Test
    public void testCreateTransactionWalletBalanceEvent() {
        classToTest.createTransactionWalletBalanceEvent("test_id_20001");
        classToTest.createTransactionWalletBalanceEvent("test_id_20002");

        Map<String, Object> consumerProps = KafkaTestUtils.consumerProps("transaction_processing", "true", embeddedKafka);
        consumerProps.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        ConsumerFactory<Integer, String> cf = new DefaultKafkaConsumerFactory<>(consumerProps);
        Consumer<Integer, String> consumer = cf.createConsumer();

        embeddedKafka.consumeFromAnEmbeddedTopic(consumer, TransactionEventsQueueNames.PROCESS_WALLET_BALANCE_TOPIC);

        ConsumerRecords<Integer, String> replies = KafkaTestUtils.getRecords(consumer);

        Assertions.assertEquals(2, replies.count());
    }
}
