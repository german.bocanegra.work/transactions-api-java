package com.gbw.transactions_api.adapter.input.listeners;

import com.gbw.transactions_api.application.ports.input.transactions.IProcessTransactionPaymentPort;
import com.gbw.transactions_api.application.ports.input.transactions.IProcessTransactionWalletBalancePort;
import com.gbw.transactions_api.application.ports.input.transactions.requests.CreateTransactionRecipientRequest;
import com.gbw.transactions_api.application.ports.input.transactions.requests.CreateTransactionRequest;
import com.gbw.transactions_api.common.exceptions.RecoverableErrorException;
import com.gbw.transactions_api.domain.entities.Transaction;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.kafka.support.Acknowledgment;

import java.time.Duration;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class SpringKafkaTransactionListenersTests {
    @InjectMocks
    private SpringKafkaTransactionListeners listeners;
    @Mock
    private IProcessTransactionPaymentPort processTransactionPaymentPort;
    @Mock
    private IProcessTransactionWalletBalancePort processTransactionWalletBalancePort;

    @Test
    public void createTransactionsEndpoint() throws Exception
    {
        var transactionValue = 27f;
        var request = new CreateTransactionRequest(
                transactionValue, new CreateTransactionRecipientRequest(
                "TestName", "TestSurname", "100001",
                "20002", "30003", "TestBank"
        )
        );

        var expectedResponse = new Transaction();

        doThrow(new RecoverableErrorException("Test exception")).when(processTransactionPaymentPort).processPayment(any(String.class));

        try {
            listeners.processPaymentListener("", new Acknowledgment() {
                @Override
                public void acknowledge() {
                    throw new RuntimeException("Ok");
                }

                @Override
                public void nack(Duration sleep) {
                    throw new RuntimeException("Expected error");
                }
            });
        }
        catch (RuntimeException e) {
            if (e.getMessage().equals("Ok")) {
                throw new RuntimeException("Test failed because payments service exception was not handled as Nack in Kafka to reattempt");
            }
            else if (e.getMessage().equals("Expected error")) {
                Assertions.assertEquals(e.getMessage(), "Expected error");
            }
        }

        verify(processTransactionPaymentPort, times(1)).processPayment(any(String.class));
    }
}
