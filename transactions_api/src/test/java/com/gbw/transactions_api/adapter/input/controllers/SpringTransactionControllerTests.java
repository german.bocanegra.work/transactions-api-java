package com.gbw.transactions_api.adapter.input.controllers;

import com.gbw.transactions_api.application.ports.input.transactions.IInitiateTransactionPort;
import com.gbw.transactions_api.application.ports.input.transactions.requests.CreateTransactionRecipientRequest;
import com.gbw.transactions_api.application.ports.input.transactions.requests.CreateTransactionRequest;
import com.gbw.transactions_api.application.ports.input.transactions.validators.ICreateTransactionValidatorPort;
import com.gbw.transactions_api.domain.entities.Transaction;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class SpringTransactionControllerTests {
    @InjectMocks
    private SpringTransactionController controller;
    @Mock
    private IInitiateTransactionPort initiateTransactionPort;
    @Mock
    private ICreateTransactionValidatorPort createTransactionValidatorPort;

    @Test
    public void createTransactionsEndpoint() throws Exception
    {
        var transactionValue = 27f;
        var request = new CreateTransactionRequest(
                transactionValue, new CreateTransactionRecipientRequest(
                "TestName", "TestSurname", "100001",
                "20002", "30003", "TestBank"
        )
        );

        var expectedResponse = new Transaction();

        when(initiateTransactionPort.initiateTransaction(any(CreateTransactionRequest.class))).thenReturn(expectedResponse);

        var response = controller.createTransaction(request);

        Assertions.assertNotNull(response);

        verify(initiateTransactionPort, times(1)).initiateTransaction(any(CreateTransactionRequest.class));
        verify(createTransactionValidatorPort, times(1)).validateCreateTransactionRequest(any(CreateTransactionRequest.class));
    }
}
