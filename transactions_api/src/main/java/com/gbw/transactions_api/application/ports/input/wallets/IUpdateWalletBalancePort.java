package com.gbw.transactions_api.application.ports.input.wallets;

import com.gbw.transactions_api.application.ports.input.wallets.responses.UpdateWalletBalanceResponse;
import com.gbw.transactions_api.common.exceptions.RecoverableErrorException;
import com.gbw.transactions_api.common.exceptions.UnrecoverableErrorException;

public interface IUpdateWalletBalancePort {
    UpdateWalletBalanceResponse updateWalletBalance(String userId, float amount)
            throws UnrecoverableErrorException, RecoverableErrorException;
}
