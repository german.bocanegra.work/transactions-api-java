package com.gbw.transactions_api.application.ports.output.events;

public interface ICreateTransactionProcessEventsPort {
    void createTransactionPaymentEvent(String transactionId);
    void createTransactionWalletBalanceEvent(String transactionId);
}
