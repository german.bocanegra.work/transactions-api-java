package com.gbw.transactions_api.application.ports.input.payments.responses;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestInfoResponse {
    @JsonProperty("status")
    private String status;
    @JsonProperty("error")
    private String error;

    public RequestInfoResponse() {
    }

    public RequestInfoResponse(String status, String error) {
        this.status = status;
        this.error = error;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
