package com.gbw.transactions_api.application.services.transactions;

import com.gbw.transactions_api.application.ports.input.transactions.IInitiateTransactionPort;
import com.gbw.transactions_api.application.ports.output.ICreateTransactionPort;
import com.gbw.transactions_api.application.ports.output.events.ICreateTransactionProcessEventsPort;
import com.gbw.transactions_api.application.ports.input.transactions.requests.CreateTransactionRequest;
import com.gbw.transactions_api.common.utilities.PercentageCalculator;
import com.gbw.transactions_api.domain.constants.TransactionStates;
import com.gbw.transactions_api.domain.entities.*;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;

@Component
public class InitiateTransactionService implements IInitiateTransactionPort {
    private final ICreateTransactionPort createTransactionPort;
    private final ICreateTransactionProcessEventsPort createTransactionProcessEventsPort;
    private final UserData userData;
    private final TransactionSettings transactionSettings;

    public InitiateTransactionService(ICreateTransactionPort createTransactionPort, ICreateTransactionProcessEventsPort createTransactionProcessEventsPort, UserData userData, TransactionSettings transactionSettings) {
        this.createTransactionPort = createTransactionPort;
        this.createTransactionProcessEventsPort = createTransactionProcessEventsPort;
        this.userData = userData;
        this.transactionSettings = transactionSettings;
    }

    @Override
    public Transaction initiateTransaction(CreateTransactionRequest request) {
        var transaction = new Transaction();

        transaction.setRecipient(new Recipient(
                request.getRecipient().getName(),
                request.getRecipient().getSurname(),
                request.getRecipient().getRoutingNumber(),
                request.getRecipient().getNationalIdNumber(),
                request.getRecipient().getAccountNumber(),
                request.getRecipient().getBankName(),
                "",
                ""
        ));

        transaction.setTransactionValue(request.getTransactionValue());

        var feePercentage = transactionSettings.getTransactionFeePercentage();
        var feeValue = PercentageCalculator.CalculatePercentageOfValue(request.getTransactionValue(), feePercentage);

        transaction.setFeePercentage(feePercentage);
        transaction.setFeeValue(feeValue);

        var transactionMinusFeeValue = transaction.getTransactionValue() - transaction.getFeeValue();
        transaction.setTransactionMinusFeeValue(transactionMinusFeeValue);

        transaction.setCreationDate(new Date());
        transaction.setCompletionDate(null);

        transaction.setRetriesCounter(0);
        transaction.setRetriesMax(transactionSettings.getTransactionPaymentRetries());
        transaction.setErrorsList(new ArrayList<>());

        transaction.setStatus(TransactionStates.PENDING);
        transaction.setSenderUserId(userData.getUserId());

        var createdTransaction = createTransactionPort.create(transaction);

        createTransactionProcessEventsPort.createTransactionPaymentEvent(createdTransaction.getId());

        return createdTransaction;
    }
}
