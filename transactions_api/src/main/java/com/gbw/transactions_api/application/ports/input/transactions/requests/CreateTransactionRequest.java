package com.gbw.transactions_api.application.ports.input.transactions.requests;

public class CreateTransactionRequest {
    private float transactionValue;
    private CreateTransactionRecipientRequest recipient;

    public CreateTransactionRequest() {
    }

    public CreateTransactionRequest(float transactionValue, CreateTransactionRecipientRequest recipient) {
        this.transactionValue = transactionValue;
        this.recipient = recipient;
    }

    public float getTransactionValue() {
        return transactionValue;
    }

    public void setTransactionValue(float transactionValue) {
        this.transactionValue = transactionValue;
    }

    public CreateTransactionRecipientRequest getRecipient() {
        return recipient;
    }

    public void setRecipient(CreateTransactionRecipientRequest recipient) {
        this.recipient = recipient;
    }
}
