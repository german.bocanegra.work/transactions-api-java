package com.gbw.transactions_api.application.ports.output;

import com.gbw.transactions_api.domain.entities.Transaction;

public interface IUpdateTransactionPort {
    Transaction update(Transaction transaction);
}
