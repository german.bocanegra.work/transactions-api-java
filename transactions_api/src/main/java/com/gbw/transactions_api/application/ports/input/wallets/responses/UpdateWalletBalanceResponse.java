package com.gbw.transactions_api.application.ports.input.wallets.responses;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UpdateWalletBalanceResponse {
    @JsonProperty("wallet_transaction_id")
    private String walletTransactionId;
    @JsonProperty("amount")
    private long amount;
    @JsonProperty("user_id")
    private int userId;

    public UpdateWalletBalanceResponse() {
    }

    public UpdateWalletBalanceResponse(String wallet_transaction_id, long amount, int user_id) {
        this.walletTransactionId = wallet_transaction_id;
        this.amount = amount;
        this.userId = user_id;
    }

    public String getWalletTransactionId() {
        return walletTransactionId;
    }

    public void setWalletTransactionId(String walletTransactionId) {
        this.walletTransactionId = walletTransactionId;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
