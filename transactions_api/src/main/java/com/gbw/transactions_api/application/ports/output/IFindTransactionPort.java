package com.gbw.transactions_api.application.ports.output;

import com.gbw.transactions_api.domain.entities.Transaction;

import java.util.UUID;

public interface IFindTransactionPort {
    Transaction find(String id);
}
