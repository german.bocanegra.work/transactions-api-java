package com.gbw.transactions_api.application.ports.input.payments.requests;

public class AccountRequest {
    private String accountNumber;
    private String currency;
    private String routingNumber;

    public AccountRequest() {
    }

    public AccountRequest(String accountNumber, String currency, String routingNumber) {
        this.accountNumber = accountNumber;
        this.currency = currency;
        this.routingNumber = routingNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getRoutingNumber() {
        return routingNumber;
    }

    public void setRoutingNumber(String routingNumber) {
        this.routingNumber = routingNumber;
    }
}
