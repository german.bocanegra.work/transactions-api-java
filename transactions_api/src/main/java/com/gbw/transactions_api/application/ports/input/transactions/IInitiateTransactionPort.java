package com.gbw.transactions_api.application.ports.input.transactions;

import com.gbw.transactions_api.application.ports.input.transactions.requests.CreateTransactionRequest;
import com.gbw.transactions_api.domain.entities.Transaction;

public interface IInitiateTransactionPort {
    Transaction initiateTransaction(CreateTransactionRequest request);
}
