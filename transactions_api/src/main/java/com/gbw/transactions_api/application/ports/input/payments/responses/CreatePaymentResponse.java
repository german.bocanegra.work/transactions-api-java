package com.gbw.transactions_api.application.ports.input.payments.responses;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreatePaymentResponse {
    @JsonProperty("requestInfo")
    private RequestInfoResponse requestInfo;
    @JsonProperty("paymentInfo")
    private PaymentInfoResponse paymentInfo;

    public CreatePaymentResponse() {
    }

    public CreatePaymentResponse(RequestInfoResponse requestInfo, PaymentInfoResponse paymentInfo) {
        this.requestInfo = requestInfo;
        this.paymentInfo = paymentInfo;
    }

    public RequestInfoResponse getRequestInfo() {
        return requestInfo;
    }

    public void setRequestInfo(RequestInfoResponse requestInfo) {
        this.requestInfo = requestInfo;
    }

    public PaymentInfoResponse getPaymentInfo() {
        return paymentInfo;
    }

    public void setPaymentInfo(PaymentInfoResponse paymentInfo) {
        this.paymentInfo = paymentInfo;
    }
}
