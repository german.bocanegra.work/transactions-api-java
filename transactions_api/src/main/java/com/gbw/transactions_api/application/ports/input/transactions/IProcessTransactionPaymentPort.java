package com.gbw.transactions_api.application.ports.input.transactions;

import com.gbw.transactions_api.common.exceptions.RecoverableErrorException;

public interface IProcessTransactionPaymentPort {
    void processPayment(String transactionId) throws RecoverableErrorException;
}
