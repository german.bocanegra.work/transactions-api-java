package com.gbw.transactions_api.application.ports.input.payments.requests;

public class SourceRequest {
    private String type;
    private SourceInformationRequest sourceInformation;
    private AccountRequest account;

    public SourceRequest() {
    }

    public SourceRequest(String type, SourceInformationRequest sourceInformation, AccountRequest account) {
        this.type = type;
        this.sourceInformation = sourceInformation;
        this.account = account;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public SourceInformationRequest getSourceInformation() {
        return sourceInformation;
    }

    public void setSourceInformation(SourceInformationRequest sourceInformation) {
        this.sourceInformation = sourceInformation;
    }

    public AccountRequest getAccount() {
        return account;
    }

    public void setAccount(AccountRequest account) {
        this.account = account;
    }
}
