package com.gbw.transactions_api.application.services.transactions.validators;

import com.gbw.transactions_api.application.ports.input.transactions.requests.CreateTransactionRequest;
import com.gbw.transactions_api.application.ports.input.transactions.validators.ICreateTransactionValidatorPort;
import com.gbw.transactions_api.common.exceptions.ServiceValidationException;
import org.springframework.stereotype.Component;

@Component
public class CreateTransactionRequestValidator implements ICreateTransactionValidatorPort {
    @Override
    public void validateCreateTransactionRequest(CreateTransactionRequest request) {
        if (request.getTransactionValue() <= 0) {
            throw new ServiceValidationException(
                    "TRANSACTION_VALUE_400",
                    "The Transaction value must be greater than 0");
        }
        else if (request.getRecipient() == null) {
            throw new ServiceValidationException(
                    "RECIPIENT_400",
                    "The recipient information must be provided");
        }
        else if (request.getRecipient().getName() == null || request.getRecipient().getName().equals("")) {
            throw new ServiceValidationException(
                    "RECIPIENT_FIRST_NAME_400",
                    "The recipient's first name must be provided");
        }
        else if (request.getRecipient().getSurname() == null || request.getRecipient().getSurname().equals("")) {
            throw new ServiceValidationException(
                    "RECIPIENT_LAST_NAME_400",
                    "The recipient's last name must be provided");
        }
        else if (request.getRecipient().getRoutingNumber() == null || request.getRecipient().getRoutingNumber().equals("")) {
            throw new ServiceValidationException(
                    "RECIPIENT_ROUTING_NUMBER_400",
                    "The recipient's routing number must be provided");
        }
        else if (request.getRecipient().getNationalIdNumber() == null || request.getRecipient().getNationalIdNumber().equals("")) {
            throw new ServiceValidationException(
                    "RECIPIENT_NATIONAL_ID_NUMBER_400",
                    "The recipient's national identification number must be provided");
        }
        else if (request.getRecipient().getAccountNumber() == null || request.getRecipient().getAccountNumber().equals("")) {
            throw new ServiceValidationException(
                    "RECIPIENT_ACCOUNT_NUMBER_400",
                    "The recipient's account number must be provided");
        }
        else if (request.getRecipient().getBankName() == null || request.getRecipient().getBankName().equals("")) {
            throw new ServiceValidationException(
                    "RECIPIENT_BANK_NAME_400",
                    "The recipient's bank name must be provided");
        }
    }
}
