package com.gbw.transactions_api.application.ports.input.payments.responses;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PaymentInfoResponse {
    @JsonProperty("amount")
    private long amount;
    @JsonProperty("id")
    private String id;

    public PaymentInfoResponse() {
    }

    public PaymentInfoResponse(long amount, String id) {
        this.amount = amount;
        this.id = id;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
