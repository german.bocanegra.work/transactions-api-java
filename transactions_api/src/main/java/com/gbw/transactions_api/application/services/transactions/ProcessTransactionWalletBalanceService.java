package com.gbw.transactions_api.application.services.transactions;

import com.gbw.transactions_api.application.ports.input.transactions.IProcessTransactionWalletBalancePort;
import com.gbw.transactions_api.application.ports.input.wallets.IUpdateWalletBalancePort;
import com.gbw.transactions_api.application.ports.output.IFindTransactionPort;
import com.gbw.transactions_api.application.ports.output.IUpdateTransactionPort;
import com.gbw.transactions_api.common.exceptions.RecoverableErrorException;
import com.gbw.transactions_api.common.exceptions.UnrecoverableErrorException;
import com.gbw.transactions_api.domain.constants.TransactionStates;
import com.gbw.transactions_api.domain.entities.UserData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.util.Date;

@Component
public class ProcessTransactionWalletBalanceService implements IProcessTransactionWalletBalancePort {
    private static final Logger log = LoggerFactory.getLogger(ProcessTransactionWalletBalanceService.class);
    private final IFindTransactionPort findTransactionPort;
    private final IUpdateTransactionPort updateTransactionPort;
    private final IUpdateWalletBalancePort updateWalletBalancePort;
    private final UserData userData;

    public ProcessTransactionWalletBalanceService(
            IFindTransactionPort findTransactionPort, IUpdateTransactionPort updateTransactionPort,
            IUpdateWalletBalancePort updateWalletBalancePort, UserData userData) {
        this.findTransactionPort = findTransactionPort;
        this.updateTransactionPort = updateTransactionPort;
        this.updateWalletBalancePort = updateWalletBalancePort;
        this.userData = userData;
    }

    @Override
    public void processWalletBalance(String transactionId) throws RecoverableErrorException{
        var transaction = findTransactionPort.find(transactionId);

        if (transaction == null) {
            log.error(MessageFormat.format("Transaction Wallet Balance processing request failed because the Transaction entry returned null - ID: {0}", transactionId));
            return;
        }

        try {
            float fullValueToWithdraw = transaction.getTransactionValue() + transaction.getFeeValue();
            var result = updateWalletBalancePort.updateWalletBalance(userData.getUserId(), fullValueToWithdraw);

            if (result.getWalletTransactionId() == null || result.getWalletTransactionId().equals("")) {
                log.warn(MessageFormat.format("Transaction Wallet Balance processing request was successful but the Balance Update transaction ID was missing in the response - ID: {0}", transactionId));
                // To do: send to an error queue to handle missing transaction IDs (this ID should not be missing)
                transaction.setWalletBalanceTrackingId("MISSING");
            }
            else {
                transaction.setWalletBalanceTrackingId(String.valueOf(result.getWalletTransactionId()));
            }
        }
        catch (RecoverableErrorException e) {
            if (transaction.canBeRetried()) {
                log.warn(MessageFormat.format("Transaction Wallet Balance processing request failed, retrying - ID: {0}", transactionId));

                transaction.setRetriesCounter(transaction.getRetriesCounter() + 1);
                transaction.getErrorsList().add(e.getMessage());
                updateTransactionPort.update(transaction);

                throw e;
            }
            else {
                log.error(MessageFormat.format("Transaction Wallet Balance processing request failed after max retries - ID: {0}", transactionId), e);
                // To do: send to an error queue to handle failed transaction payments
                transaction.setStatus(TransactionStates.BALANCE_UPDATE_FAILED);
                transaction.getErrorsList().add(e.getMessage());
                updateTransactionPort.update(transaction);
                return;
            }
        }
        catch (UnrecoverableErrorException e) {
            log.error(MessageFormat.format("Transaction Wallet Balance processing request unexpectedly failed - ID: {0}", transactionId), e);
            // To do: send to an error queue to handle failed transaction payments
            transaction.setStatus(TransactionStates.BALANCE_UPDATE_FAILED);
            transaction.getErrorsList().add(e.getMessage());
            updateTransactionPort.update(transaction);
            return;
        }

        transaction.setStatus(TransactionStates.BALANCE_UPDATED);
        transaction.setCompletionDate(new Date());
        updateTransactionPort.update(transaction);
    }
}
