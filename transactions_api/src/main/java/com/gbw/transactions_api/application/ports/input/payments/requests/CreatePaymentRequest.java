package com.gbw.transactions_api.application.ports.input.payments.requests;

public class CreatePaymentRequest {
    private SourceRequest source;
    private DestinationRequest destination;
    private float amount;

    public CreatePaymentRequest() {
    }

    public CreatePaymentRequest(SourceRequest source, DestinationRequest destination, float amount) {
        this.source = source;
        this.destination = destination;
        this.amount = amount;
    }

    public SourceRequest getSource() {
        return source;
    }

    public void setSource(SourceRequest source) {
        this.source = source;
    }

    public DestinationRequest getDestination() {
        return destination;
    }

    public void setDestination(DestinationRequest destination) {
        this.destination = destination;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }
}
