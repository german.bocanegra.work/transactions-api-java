package com.gbw.transactions_api.application.services.payments;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gbw.transactions_api.application.ports.input.payments.ITransferFundsPort;
import com.gbw.transactions_api.application.ports.input.payments.requests.CreatePaymentRequest;
import com.gbw.transactions_api.application.ports.input.payments.requests.DestinationRequest;
import com.gbw.transactions_api.application.ports.input.payments.requests.SourceRequest;
import com.gbw.transactions_api.application.ports.input.payments.responses.CreatePaymentResponse;
import com.gbw.transactions_api.common.exceptions.RecoverableErrorException;
import com.gbw.transactions_api.common.exceptions.UnrecoverableErrorException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

@Component
public class TransferFundsService implements ITransferFundsPort {
    @Value("${transaction-api.hosts.payments-api-host}")
    private String serviceHost;

    @Override
    public CreatePaymentResponse transferFunds(SourceRequest sourceDetails, DestinationRequest destinationDetails, float amount)
        throws UnrecoverableErrorException, RecoverableErrorException{
        RestTemplate restTemplate = new RestTemplate();

        var requestBody = new CreatePaymentRequest(
                sourceDetails,
                destinationDetails,
                amount);

        try {
            return restTemplate.postForObject(serviceHost + "/api/v1/payments", requestBody, CreatePaymentResponse.class);
        }
        catch (ResourceAccessException e) {
            throw new RecoverableErrorException("Unexpected error from Funds Transfer API");
        }
        catch (HttpClientErrorException e) {
            var errorResponseString = e.getResponseBodyAsString();

            if (e.getRawStatusCode() == 400) {
                throw new UnrecoverableErrorException("Invalid request: " + errorResponseString);
            }
            else if (e.getRawStatusCode() == 500) {
                CreatePaymentResponse errorResponse;
                ObjectMapper objectMapper = new ObjectMapper();

                try {
                    errorResponse = objectMapper.readValue(errorResponseString, CreatePaymentResponse.class);

                    if (errorResponse != null && errorResponse.getRequestInfo() != null && errorResponse.getRequestInfo().getError() != null) {
                        var errorMessage = errorResponse.getRequestInfo().getError();

                        if (errorMessage.equals("bank rejected payment")) {
                            throw new UnrecoverableErrorException("Payment was rejected by the bank");
                        }
                        else if (errorMessage.equals("timeout")) {
                            throw new RecoverableErrorException("Unexpected timeout from Funds Transfer API");
                        }
                        else {
                            throw new RecoverableErrorException(errorMessage);
                        }
                    }
                    else {
                        throw new RecoverableErrorException("Unexpected error message from Funds Transfer API: " + errorResponseString);
                    }

                } catch (IOException e2) {
                    throw new RecoverableErrorException("Unexpected error response body from Funds Transfer API: " + errorResponseString);
                }

            }
            else {
                throw new RecoverableErrorException("Unexpected error from Funds Transfer API");
            }
        }
    }
}
