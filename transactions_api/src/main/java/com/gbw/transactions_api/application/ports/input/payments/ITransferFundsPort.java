package com.gbw.transactions_api.application.ports.input.payments;

import com.gbw.transactions_api.application.ports.input.payments.requests.DestinationRequest;
import com.gbw.transactions_api.application.ports.input.payments.requests.SourceRequest;
import com.gbw.transactions_api.application.ports.input.payments.responses.CreatePaymentResponse;
import com.gbw.transactions_api.common.exceptions.RecoverableErrorException;
import com.gbw.transactions_api.common.exceptions.UnrecoverableErrorException;

public interface ITransferFundsPort {
    CreatePaymentResponse transferFunds(SourceRequest sourceDetails, DestinationRequest destinationDetails, float amount)
            throws UnrecoverableErrorException, RecoverableErrorException;
}
