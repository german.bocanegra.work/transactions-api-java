package com.gbw.transactions_api.application.ports.input.transactions.validators;

import com.gbw.transactions_api.application.ports.input.transactions.requests.CreateTransactionRequest;

public interface ICreateTransactionValidatorPort {
    void validateCreateTransactionRequest(CreateTransactionRequest request);
}
