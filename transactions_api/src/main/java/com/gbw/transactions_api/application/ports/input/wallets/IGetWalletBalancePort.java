package com.gbw.transactions_api.application.ports.input.wallets;

public interface IGetWalletBalancePort {
    long getWalletBalance(String userId);
}
