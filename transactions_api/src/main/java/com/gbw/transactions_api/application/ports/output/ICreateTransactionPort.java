package com.gbw.transactions_api.application.ports.output;

import com.gbw.transactions_api.domain.entities.Transaction;

public interface ICreateTransactionPort {
    Transaction create(Transaction transaction);
}
