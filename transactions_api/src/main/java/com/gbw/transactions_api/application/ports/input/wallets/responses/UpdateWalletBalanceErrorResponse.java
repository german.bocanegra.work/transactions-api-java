package com.gbw.transactions_api.application.ports.input.wallets.responses;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UpdateWalletBalanceErrorResponse {
    @JsonProperty("code")
    private String code;
    @JsonProperty("message")
    private String message;

    public UpdateWalletBalanceErrorResponse() {
    }

    public UpdateWalletBalanceErrorResponse(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
