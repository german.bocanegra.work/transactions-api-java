package com.gbw.transactions_api.application.ports.input.payments.requests;

public class SourceInformationRequest {
    private String name;

    public SourceInformationRequest() {
    }

    public SourceInformationRequest(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
