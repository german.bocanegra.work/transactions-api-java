package com.gbw.transactions_api.application.ports.input.payments.requests;

public class DestinationRequest {
    private String name;
    private AccountRequest account;

    public DestinationRequest() {
    }

    public DestinationRequest(String name, AccountRequest account) {
        this.name = name;
        this.account = account;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AccountRequest getAccount() {
        return account;
    }

    public void setAccount(AccountRequest account) {
        this.account = account;
    }
}
