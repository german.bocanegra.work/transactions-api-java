package com.gbw.transactions_api.application.services.wallets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gbw.transactions_api.application.ports.input.wallets.IUpdateWalletBalancePort;
import com.gbw.transactions_api.application.ports.input.wallets.requests.UpdateWalletBalanceRequest;
import com.gbw.transactions_api.application.ports.input.wallets.responses.UpdateWalletBalanceErrorResponse;
import com.gbw.transactions_api.application.ports.input.wallets.responses.UpdateWalletBalanceResponse;
import com.gbw.transactions_api.common.exceptions.RecoverableErrorException;
import com.gbw.transactions_api.common.exceptions.UnrecoverableErrorException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

@Component
public class UpdateWalletBalanceService implements IUpdateWalletBalancePort {
    @Value("${transaction-api.hosts.wallet-api-host}")
    private String serviceHost;

    @Override
    public UpdateWalletBalanceResponse updateWalletBalance(String userId, float amount)
            throws UnrecoverableErrorException, RecoverableErrorException{
        RestTemplate restTemplate = new RestTemplate();

        var requestBody = new UpdateWalletBalanceRequest(
                amount,
                Integer.parseInt(userId));

        try {
            return restTemplate.postForObject(serviceHost + "/wallets/transactions", requestBody, UpdateWalletBalanceResponse.class);
        }
        catch (ResourceAccessException e) {
            throw new RecoverableErrorException("Unexpected error from Wallet API");
        }
        catch (HttpClientErrorException e) {
            var errorResponseString = e.getResponseBodyAsString();

            if (e.getRawStatusCode() == 400) {
                throw new UnrecoverableErrorException("Invalid request: " + errorResponseString);
            }
            else if (e.getRawStatusCode() == 404) {
                if (!errorResponseString.equals("")) {
                    UpdateWalletBalanceErrorResponse errorResponse;
                    ObjectMapper objectMapper = new ObjectMapper();

                    try {
                        errorResponse = objectMapper.readValue(errorResponseString, UpdateWalletBalanceErrorResponse.class);

                        if (errorResponse != null && errorResponse.getCode() != null) {
                            var errorCode = errorResponse.getCode();
                            var errorMessage = errorResponse.getMessage();

                            if (errorCode.equals("INVALID_USER")) {
                                throw new UnrecoverableErrorException("Wallet API User not found: " + errorMessage);
                            }
                            else {
                                throw new RecoverableErrorException(errorMessage);
                            }
                        }
                        else {
                            throw new RecoverableErrorException("Unexpected error message from Wallet API: " + errorResponseString);
                        }

                    } catch (IOException e2) {
                        throw new RecoverableErrorException("Unexpected error response body from Wallet API: " + errorResponseString);
                    }
                }

                throw new UnrecoverableErrorException("Invalid request: " + errorResponseString);
            }
            else if (e.getRawStatusCode() == 500) {
                UpdateWalletBalanceErrorResponse errorResponse;
                ObjectMapper objectMapper = new ObjectMapper();

                try {
                    errorResponse = objectMapper.readValue(errorResponseString, UpdateWalletBalanceErrorResponse.class);

                    if (errorResponse != null && errorResponse.getCode() != null) {
                        var errorCode = errorResponse.getCode();
                        var errorMessage = errorResponse.getMessage();

                        if (errorCode.equals("GENERIC_ERROR")) {
                            throw new UnrecoverableErrorException("General error from Wallet API: " + errorMessage);
                        }
                        else {
                            throw new RecoverableErrorException(errorMessage);
                        }
                    }
                    else {
                        throw new RecoverableErrorException("Unexpected error message from Wallet API: " + errorResponseString);
                    }

                } catch (IOException e2) {
                    throw new RecoverableErrorException("Unexpected error response body from Wallet API: " + errorResponseString);
                }

            }
            else {
                throw new RecoverableErrorException("Unexpected error from Wallet API");
            }
        }
    }
}
