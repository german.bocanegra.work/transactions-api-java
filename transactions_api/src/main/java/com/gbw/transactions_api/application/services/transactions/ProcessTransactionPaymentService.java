package com.gbw.transactions_api.application.services.transactions;

import com.gbw.transactions_api.application.ports.input.payments.ITransferFundsPort;
import com.gbw.transactions_api.application.ports.input.transactions.IProcessTransactionPaymentPort;
import com.gbw.transactions_api.application.ports.output.IFindTransactionPort;
import com.gbw.transactions_api.application.ports.output.IUpdateTransactionPort;
import com.gbw.transactions_api.application.ports.output.events.ICreateTransactionProcessEventsPort;
import com.gbw.transactions_api.application.ports.input.payments.requests.AccountRequest;
import com.gbw.transactions_api.application.ports.input.payments.requests.DestinationRequest;
import com.gbw.transactions_api.application.ports.input.payments.requests.SourceInformationRequest;
import com.gbw.transactions_api.application.ports.input.payments.requests.SourceRequest;
import com.gbw.transactions_api.common.exceptions.RecoverableErrorException;
import com.gbw.transactions_api.common.exceptions.UnrecoverableErrorException;
import com.gbw.transactions_api.domain.constants.TransactionStates;
import com.gbw.transactions_api.domain.entities.SourceAccountData;
import com.gbw.transactions_api.domain.entities.TransactionSettings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;

@Component
public class ProcessTransactionPaymentService implements IProcessTransactionPaymentPort {
    private static final Logger log = LoggerFactory.getLogger(ProcessTransactionPaymentService.class);
    private final IFindTransactionPort findTransactionPort;
    private final IUpdateTransactionPort updateTransactionPort;
    private final ITransferFundsPort transferFundsPort;
    private final ICreateTransactionProcessEventsPort createTransactionProcessEventsPort;
    private final SourceAccountData sourceAccountData;
    private final TransactionSettings transactionSettings;

    public ProcessTransactionPaymentService(
            IFindTransactionPort findTransactionPort, IUpdateTransactionPort updateTransactionPort,
            ITransferFundsPort transferFundsPort, ICreateTransactionProcessEventsPort createTransactionProcessEventsPort,
            SourceAccountData sourceAccountData, TransactionSettings transactionSettings) {
        this.findTransactionPort = findTransactionPort;
        this.updateTransactionPort = updateTransactionPort;
        this.transferFundsPort = transferFundsPort;
        this.createTransactionProcessEventsPort = createTransactionProcessEventsPort;
        this.sourceAccountData = sourceAccountData;
        this.transactionSettings = transactionSettings;
    }

    @Override
    public void processPayment(String transactionId) throws RecoverableErrorException{
        var transaction = findTransactionPort.find(transactionId);

        if (transaction == null) {
            log.error(MessageFormat.format("Transaction Payment processing request failed because the Transaction entry returned null - ID: {0}", transactionId));
            return;
        }

        try {
            var recipient = transaction.getRecipient();

            var result = transferFundsPort.transferFunds(
                    new SourceRequest(
                            sourceAccountData.getSourceType(),
                            new SourceInformationRequest(sourceAccountData.getSourceInformationName()),
                            new AccountRequest(
                                    sourceAccountData.getSourceAccountNumber(),
                                    sourceAccountData.getSourceCurrency(),
                                    sourceAccountData.getSourceRoutingNumber())),
                    new DestinationRequest(
                            recipient.getName() + " " + recipient.getSurname(),
                            new AccountRequest(
                                    recipient.getAccountNumber(),
                                    sourceAccountData.getSourceCurrency(),
                                    recipient.getRoutingNumber())),
                    transaction.getTransactionMinusFeeValue()
            );

            if (result.getPaymentInfo() == null || result.getPaymentInfo().getId().equals("")) {
                log.warn(MessageFormat.format("Transaction Payment processing request was successful but the Payment transaction ID was missing in the response - ID: {0}", transactionId));
                // To do: send to an error queue to handle missing transaction IDs (this ID should not be missing)
                transaction.setTransferFundsTrackingId("MISSING");
            }
            else {
                transaction.setTransferFundsTrackingId(result.getPaymentInfo().getId());
            }
        }
        catch (RecoverableErrorException e) {
            if (transaction.canBeRetried()) {
                log.warn(MessageFormat.format("Transaction Payment processing request failed, retrying - ID: {0}", transactionId));

                transaction.setRetriesCounter(transaction.getRetriesCounter() + 1);
                transaction.getErrorsList().add(e.getMessage());
                updateTransactionPort.update(transaction);

                throw e;
            }
            else {
                log.error(MessageFormat.format("Transaction Payment processing request failed after max retries - ID: {0}", transactionId), e);
                // To do: send to an error queue to handle failed transaction payments
                transaction.setStatus(TransactionStates.PAYMENT_FAILED);
                transaction.getErrorsList().add(e.getMessage());
                updateTransactionPort.update(transaction);
                return;
            }
        }
        catch (UnrecoverableErrorException e) {
            log.error(MessageFormat.format("Transaction Payment processing request unexpectedly failed - ID: {0}", transactionId), e);
            // To do: send to an error queue to handle failed transaction payments
            transaction.setStatus(TransactionStates.PAYMENT_FAILED);
            transaction.getErrorsList().add(e.getMessage());
            updateTransactionPort.update(transaction);
            return;
        }

        transaction.setStatus(TransactionStates.PAYMENT_DELIVERED);
        transaction.setRetriesMax(transactionSettings.getTransactionWalletBalanceRetries());
        transaction.setRetriesCounter(0);
        updateTransactionPort.update(transaction);

        createTransactionProcessEventsPort.createTransactionWalletBalanceEvent(transaction.getId());
    }
}
