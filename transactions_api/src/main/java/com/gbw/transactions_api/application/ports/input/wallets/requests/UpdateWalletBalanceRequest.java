package com.gbw.transactions_api.application.ports.input.wallets.requests;

public class UpdateWalletBalanceRequest {
    private float amount;
    private int user_id;

    public UpdateWalletBalanceRequest() {
    }

    public UpdateWalletBalanceRequest(float amount, int user_id) {
        this.amount = amount;
        this.user_id = user_id;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
}
