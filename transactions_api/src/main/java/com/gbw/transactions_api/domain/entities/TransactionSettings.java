package com.gbw.transactions_api.domain.entities;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class TransactionSettings {

    @Value("${transaction-api.transaction-settings.fee-percentage}")
    private int transactionFeePercentage;
    @Value("${transaction-api.transaction-settings.payment-retries}")
    private int transactionPaymentRetries;
    @Value("${transaction-api.transaction-settings.wallet-balance-retries}")
    private int transactionWalletBalanceRetries;

    public TransactionSettings() {
    }

    public TransactionSettings(int transactionFeePercentage, int transactionPaymentRetries, int transactionWalletBalanceRetries) {
        this.transactionFeePercentage = transactionFeePercentage;
        this.transactionPaymentRetries = transactionPaymentRetries;
        this.transactionWalletBalanceRetries = transactionWalletBalanceRetries;
    }

    public int getTransactionFeePercentage() {
        return transactionFeePercentage;
    }

    public void setTransactionFeePercentage(int transactionFeePercentage) {
        this.transactionFeePercentage = transactionFeePercentage;
    }

    public int getTransactionPaymentRetries() {
        return transactionPaymentRetries;
    }

    public void setTransactionPaymentRetries(int transactionPaymentRetries) {
        this.transactionPaymentRetries = transactionPaymentRetries;
    }

    public int getTransactionWalletBalanceRetries() {
        return transactionWalletBalanceRetries;
    }

    public void setTransactionWalletBalanceRetries(int transactionWalletBalanceRetries) {
        this.transactionWalletBalanceRetries = transactionWalletBalanceRetries;
    }
}
