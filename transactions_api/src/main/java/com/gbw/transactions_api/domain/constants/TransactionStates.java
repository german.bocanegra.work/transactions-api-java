package com.gbw.transactions_api.domain.constants;

public class TransactionStates {
    public static final String PENDING = "Pending";
    public static final String PAYMENT_DELIVERED = "PaymentDelivered";
    public static final String BALANCE_UPDATED = "BalanceUpdated";

    public static final String PAYMENT_FAILED = "PaymentFailed";
    public static final String BALANCE_UPDATE_FAILED = "BalanceUpdateFailed";
}
