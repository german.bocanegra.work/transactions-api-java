package com.gbw.transactions_api.domain.entities;

import java.util.Date;
import java.util.List;

public class Transaction {
    private String id;
    private String status;
    private float transactionValue;
    private int feePercentage;
    private float feeValue;
    private float transactionMinusFeeValue;
    private Date creationDate;
    private Date completionDate;
    private String senderUserId;
    private String transferFundsTrackingId;
    private String walletBalanceTrackingId;
    private int retriesCounter;
    private int retriesMax;
    private List<String> errorsList;
    private Recipient recipient;

    public boolean canBeRetried() {
        return retriesCounter < retriesMax;
    }

    public Transaction() {
    }

    public Transaction(String id, String status, float transactionValue, int feePercentage, float feeValue, float transactionMinusFeeValue, Date creationDate, Date completionDate, String senderUserId, String transferFundsTrackingId, String walletBalanceTrackingId, int retriesCounter, int retriesMax, List<String> errorsList, Recipient recipient) {
        this.id = id;
        this.status = status;
        this.transactionValue = transactionValue;
        this.feePercentage = feePercentage;
        this.feeValue = feeValue;
        this.transactionMinusFeeValue = transactionMinusFeeValue;
        this.creationDate = creationDate;
        this.completionDate = completionDate;
        this.senderUserId = senderUserId;
        this.transferFundsTrackingId = transferFundsTrackingId;
        this.walletBalanceTrackingId = walletBalanceTrackingId;
        this.retriesCounter = retriesCounter;
        this.retriesMax = retriesMax;
        this.errorsList = errorsList;
        this.recipient = recipient;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public float getTransactionValue() {
        return transactionValue;
    }

    public void setTransactionValue(float transactionValue) {
        this.transactionValue = transactionValue;
    }

    public int getFeePercentage() {
        return feePercentage;
    }

    public void setFeePercentage(int feePercentage) {
        this.feePercentage = feePercentage;
    }

    public float getFeeValue() {
        return feeValue;
    }

    public void setFeeValue(float feeValue) {
        this.feeValue = feeValue;
    }

    public float getTransactionMinusFeeValue() {
        return transactionMinusFeeValue;
    }

    public void setTransactionMinusFeeValue(float transactionMinusFeeValue) {
        this.transactionMinusFeeValue = transactionMinusFeeValue;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(Date completionDate) {
        this.completionDate = completionDate;
    }

    public String getSenderUserId() {
        return senderUserId;
    }

    public void setSenderUserId(String senderUserId) {
        this.senderUserId = senderUserId;
    }

    public String getTransferFundsTrackingId() {
        return transferFundsTrackingId;
    }

    public void setTransferFundsTrackingId(String transferFundsTrackingId) {
        this.transferFundsTrackingId = transferFundsTrackingId;
    }

    public String getWalletBalanceTrackingId() {
        return walletBalanceTrackingId;
    }

    public void setWalletBalanceTrackingId(String walletBalanceTrackingId) {
        this.walletBalanceTrackingId = walletBalanceTrackingId;
    }

    public int getRetriesCounter() {
        return retriesCounter;
    }

    public void setRetriesCounter(int retriesCounter) {
        this.retriesCounter = retriesCounter;
    }

    public int getRetriesMax() {
        return retriesMax;
    }

    public void setRetriesMax(int retriesMax) {
        this.retriesMax = retriesMax;
    }

    public List<String> getErrorsList() {
        return errorsList;
    }

    public void setErrorsList(List<String> errorsList) {
        this.errorsList = errorsList;
    }

    public Recipient getRecipient() {
        return recipient;
    }

    public void setRecipient(Recipient recipient) {
        this.recipient = recipient;
    }
}
