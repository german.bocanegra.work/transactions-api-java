package com.gbw.transactions_api.domain.entities;

import org.springframework.stereotype.Component;

@Component
public class UserData {
    private String userId;

    public UserData() {
        // Note: this ID is set here as a mock, this ID should come from the request
        // Note: this ID should be populated by a filter that runs before the controller
        // To do: replace this ID mock with the appropriate source of the UserID
        this.userId = "1000";
    }

    public UserData(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
