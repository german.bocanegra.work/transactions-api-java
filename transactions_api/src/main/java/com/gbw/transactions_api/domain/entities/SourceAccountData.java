package com.gbw.transactions_api.domain.entities;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SourceAccountData {
    @Value("${transaction-api.source-account-information.type}")
    private String sourceType;
    @Value("${transaction-api.source-account-information.source-information-name}")
    private String sourceInformationName;
    @Value("${transaction-api.source-account-information.source-account-number}")
    private String sourceAccountNumber;
    @Value("${transaction-api.source-account-information.source-currency}")
    private String sourceCurrency;
    @Value("${transaction-api.source-account-information.source-routing-number}")
    private String sourceRoutingNumber;

    public SourceAccountData() {
    }

    public SourceAccountData(String sourceType, String sourceInformationName, String sourceAccountNumber, String sourceCurrency, String sourceRoutingNumber) {
        this.sourceType = sourceType;
        this.sourceInformationName = sourceInformationName;
        this.sourceAccountNumber = sourceAccountNumber;
        this.sourceCurrency = sourceCurrency;
        this.sourceRoutingNumber = sourceRoutingNumber;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public String getSourceInformationName() {
        return sourceInformationName;
    }

    public void setSourceInformationName(String sourceInformationName) {
        this.sourceInformationName = sourceInformationName;
    }

    public String getSourceAccountNumber() {
        return sourceAccountNumber;
    }

    public void setSourceAccountNumber(String sourceAccountNumber) {
        this.sourceAccountNumber = sourceAccountNumber;
    }

    public String getSourceCurrency() {
        return sourceCurrency;
    }

    public void setSourceCurrency(String sourceCurrency) {
        this.sourceCurrency = sourceCurrency;
    }

    public String getSourceRoutingNumber() {
        return sourceRoutingNumber;
    }

    public void setSourceRoutingNumber(String sourceRoutingNumber) {
        this.sourceRoutingNumber = sourceRoutingNumber;
    }
}
