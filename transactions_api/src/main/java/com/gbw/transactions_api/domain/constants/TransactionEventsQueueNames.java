package com.gbw.transactions_api.domain.constants;

public class TransactionEventsQueueNames {
    public static final String PROCESS_PAYMENT_TOPIC = "process-payment";
    public static final String PROCESS_WALLET_BALANCE_TOPIC = "process-wallet-balance";
}
