package com.gbw.transactions_api.common.exceptions;

/**
 * This exception represents a persistent error, meaning that repeating the same exact action would continue to fail
 */
public class UnrecoverableErrorException extends Exception{
    public UnrecoverableErrorException(String errorMessage) {
        super(errorMessage);
    }
}
