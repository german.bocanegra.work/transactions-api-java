package com.gbw.transactions_api.common.exceptions;

public class CodedResponseException extends RuntimeException{
    private String ErrorCode;
    private String ErrorMessage;

    public CodedResponseException(String errorCode, String errorMessage) {
        super(errorMessage);

        ErrorCode = errorCode;
        ErrorMessage = errorMessage;
    }

    public String getErrorCode() {
        return ErrorCode;
    }

    public void setErrorCode(String errorCode) {
        ErrorCode = errorCode;
    }

    public String getErrorMessage() {
        return ErrorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        ErrorMessage = errorMessage;
    }
}
