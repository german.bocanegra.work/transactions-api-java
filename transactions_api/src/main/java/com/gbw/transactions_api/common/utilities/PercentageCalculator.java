package com.gbw.transactions_api.common.utilities;

public class PercentageCalculator {
    public static float CalculatePercentageOfValue(float value, int percentage) {
        return value * ((float) percentage / 100f);
    }
}
