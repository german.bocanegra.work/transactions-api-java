package com.gbw.transactions_api.common.exceptions;

public class ServiceResourceNotFoundException extends CodedResponseException {
    public ServiceResourceNotFoundException(String errorCode, String errorMessage) {
        super(errorCode, errorMessage);
    }
}
