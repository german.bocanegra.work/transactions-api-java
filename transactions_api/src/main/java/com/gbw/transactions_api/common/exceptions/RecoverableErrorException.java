package com.gbw.transactions_api.common.exceptions;

/**
 * This exception represents a transient error, meaning that repeating the same exact action should work at a later time
 */
public class RecoverableErrorException extends Exception{
    public RecoverableErrorException(String errorMessage) {
        super(errorMessage);
    }
}
