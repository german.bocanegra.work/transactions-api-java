package com.gbw.transactions_api.common.exceptions;

public class ServiceValidationException extends CodedResponseException{
    public ServiceValidationException(String errorCode, String errorMessage) {
        super(errorCode, errorMessage);
    }
}
