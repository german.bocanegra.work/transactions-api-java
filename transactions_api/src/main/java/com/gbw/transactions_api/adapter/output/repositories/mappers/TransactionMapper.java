package com.gbw.transactions_api.adapter.output.repositories.mappers;

import com.gbw.transactions_api.adapter.output.repositories.entities.RecipientDTO;
import com.gbw.transactions_api.adapter.output.repositories.entities.TransactionDTO;
import com.gbw.transactions_api.domain.entities.Recipient;
import com.gbw.transactions_api.domain.entities.Transaction;

public class TransactionMapper {
    public static TransactionDTO Map(Transaction transaction) {
        var result = new TransactionDTO(
                transaction.getId(),
                transaction.getStatus(),
                transaction.getTransactionValue(),
                transaction.getFeePercentage(),
                transaction.getFeeValue(),
                transaction.getTransactionMinusFeeValue(),
                transaction.getCreationDate(),
                transaction.getCompletionDate(),
                transaction.getSenderUserId(),
                transaction.getTransferFundsTrackingId(),
                transaction.getWalletBalanceTrackingId(),
                transaction.getRetriesCounter(),
                transaction.getRetriesMax(),
                transaction.getErrorsList(),
                null
        );

        if (transaction.getRecipient() != null) {
            result.setRecipient(new RecipientDTO(
                    transaction.getRecipient().getName(),
                    transaction.getRecipient().getSurname(),
                    transaction.getRecipient().getRoutingNumber(),
                    transaction.getRecipient().getNationalIdNumber(),
                    transaction.getRecipient().getAccountNumber(),
                    transaction.getRecipient().getBankName(),
                    transaction.getRecipient().getSavedRecipientId(),
                    transaction.getRecipient().getSavedRecipientAlias()
            ));
        }

        return result;
    }

    public static Transaction Map(TransactionDTO transaction) {
        var result = new Transaction(
                transaction.getId(),
                transaction.getStatus(),
                transaction.getTransactionValue(),
                transaction.getFeePercentage(),
                transaction.getFeeValue(),
                transaction.getTransactionMinusFeeValue(),
                transaction.getCreationDate(),
                transaction.getCompletionDate(),
                transaction.getSenderUserId(),
                transaction.getTransferFundsTrackingId(),
                transaction.getWalletBalanceTrackingId(),
                transaction.getRetriesCounter(),
                transaction.getRetriesMax(),
                transaction.getErrorsList(),
                null
        );

        if (transaction.getRecipient() != null) {
            result.setRecipient(new Recipient(
                    transaction.getRecipient().getName(),
                    transaction.getRecipient().getSurname(),
                    transaction.getRecipient().getRoutingNumber(),
                    transaction.getRecipient().getNationalIdNumber(),
                    transaction.getRecipient().getAccountNumber(),
                    transaction.getRecipient().getBankName(),
                    transaction.getRecipient().getSavedRecipientId(),
                    transaction.getRecipient().getSavedRecipientAlias()
            ));
        }

        return result;
    }
}
