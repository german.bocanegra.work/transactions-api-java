package com.gbw.transactions_api.adapter.output.repositories.entities;

public class RecipientDTO {
    private String name;
    private String surname;
    private String routingNumber;
    private String nationalIdNumber;
    private String accountNumber;
    private String bankName;
    private String savedRecipientId;

    private String savedRecipientAlias;

    public RecipientDTO() {
    }

    public RecipientDTO(String name, String surname, String routingNumber, String nationalIdNumber, String accountNumber, String bankName, String savedRecipientId, String savedRecipientAlias) {
        this.name = name;
        this.surname = surname;
        this.routingNumber = routingNumber;
        this.nationalIdNumber = nationalIdNumber;
        this.accountNumber = accountNumber;
        this.bankName = bankName;
        this.savedRecipientId = savedRecipientId;
        this.savedRecipientAlias = savedRecipientAlias;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getRoutingNumber() {
        return routingNumber;
    }

    public void setRoutingNumber(String routingNumber) {
        this.routingNumber = routingNumber;
    }

    public String getNationalIdNumber() {
        return nationalIdNumber;
    }

    public void setNationalIdNumber(String nationalIdNumber) {
        this.nationalIdNumber = nationalIdNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getSavedRecipientId() {
        return savedRecipientId;
    }

    public void setSavedRecipientId(String savedRecipientId) {
        this.savedRecipientId = savedRecipientId;
    }

    public String getSavedRecipientAlias() {
        return savedRecipientAlias;
    }

    public void setSavedRecipientAlias(String savedRecipientAlias) {
        this.savedRecipientAlias = savedRecipientAlias;
    }
}
