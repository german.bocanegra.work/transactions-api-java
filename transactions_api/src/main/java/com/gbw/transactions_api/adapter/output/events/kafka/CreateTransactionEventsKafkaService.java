package com.gbw.transactions_api.adapter.output.events.kafka;

import com.gbw.transactions_api.application.ports.output.events.ICreateTransactionProcessEventsPort;
import com.gbw.transactions_api.domain.constants.TransactionEventsQueueNames;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class CreateTransactionEventsKafkaService implements ICreateTransactionProcessEventsPort {
    private final KafkaTemplate<Integer, String> template;

    public CreateTransactionEventsKafkaService(KafkaTemplate<Integer, String> template) {
        this.template = template;
    }

    @Override
    public void createTransactionPaymentEvent(String transactionId) {
        template.send(TransactionEventsQueueNames.PROCESS_PAYMENT_TOPIC, transactionId);
    }

    @Override
    public void createTransactionWalletBalanceEvent(String transactionId) {
        template.send(TransactionEventsQueueNames.PROCESS_WALLET_BALANCE_TOPIC, transactionId);
    }
}
