package com.gbw.transactions_api.adapter.output.repositories.accessors.mongodb;

import com.gbw.transactions_api.adapter.output.repositories.entities.TransactionDTO;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ITransactionMongoDbAccessorRepository  extends MongoRepository<TransactionDTO, String> {

}
