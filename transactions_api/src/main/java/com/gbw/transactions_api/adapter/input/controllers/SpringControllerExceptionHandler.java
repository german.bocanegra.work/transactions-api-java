package com.gbw.transactions_api.adapter.input.controllers;

import com.gbw.transactions_api.application.services.transactions.ProcessTransactionPaymentService;
import com.gbw.transactions_api.common.exceptions.ServiceResourceNotFoundException;
import com.gbw.transactions_api.common.exceptions.ServiceValidationException;
import com.gbw.transactions_api.common.responses.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletResponse;

@RestControllerAdvice
public class SpringControllerExceptionHandler {
    private static final Logger log = LoggerFactory.getLogger(ProcessTransactionPaymentService.class);

    @ExceptionHandler(value = {ServiceValidationException.class})
    public ErrorResponse serviceValidationExceptionErrorHandler(HttpServletResponse response, ServiceValidationException e) {
        response.setStatus(HttpStatus.BAD_REQUEST.value());
        return new ErrorResponse(e.getErrorCode(), e.getErrorMessage());
    }

    @ExceptionHandler(value = {ServiceResourceNotFoundException.class})
    public ErrorResponse serviceResourceNotFoundExceptionErrorHandler(HttpServletResponse response, ServiceResourceNotFoundException e) {
        response.setStatus(HttpStatus.NOT_FOUND.value());
        return new ErrorResponse(e.getErrorCode(), e.getErrorMessage());
    }

    @ExceptionHandler(value = {Exception.class, RuntimeException.class})
    public ErrorResponse fallbackErrorHandler(HttpServletResponse response, Exception e) {
        log.error("SERVER_ERROR", e);

        response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        return new ErrorResponse("SERVER_ERROR", "An internal server error has arisen, please contact the administrator");
    }
}
