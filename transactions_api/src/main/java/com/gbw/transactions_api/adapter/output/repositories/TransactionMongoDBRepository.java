package com.gbw.transactions_api.adapter.output.repositories;

import com.gbw.transactions_api.adapter.output.repositories.accessors.mongodb.ITransactionMongoDbAccessorRepository;
import com.gbw.transactions_api.adapter.output.repositories.mappers.TransactionMapper;
import com.gbw.transactions_api.application.ports.output.ICreateTransactionPort;
import com.gbw.transactions_api.application.ports.output.IFindTransactionPort;
import com.gbw.transactions_api.application.ports.output.IUpdateTransactionPort;
import com.gbw.transactions_api.domain.entities.Transaction;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class TransactionMongoDBRepository implements IFindTransactionPort, ICreateTransactionPort, IUpdateTransactionPort {
    private final ITransactionMongoDbAccessorRepository transactionRepository;

    public TransactionMongoDBRepository(ITransactionMongoDbAccessorRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    @Override
    public Transaction find(String id) {
        var queryResult = transactionRepository.findById(id);
        if (queryResult.isEmpty()) {
            return null;
        }

        return TransactionMapper.Map(queryResult.get());
    }

    @Override
    public Transaction create(Transaction transaction) {
        var mappedRequest = TransactionMapper.Map(transaction);

        transaction.setId(UUID.randomUUID().toString());
        var insertResult = transactionRepository.insert(mappedRequest);

        return TransactionMapper.Map(insertResult);
    }

    @Override
    public Transaction update(Transaction transaction) {
        var queryResult = transactionRepository.findById(transaction.getId());
        if (queryResult.isEmpty()) {
            return null;
        }

        var mappedRequest = TransactionMapper.Map(transaction);
        var updateResult = transactionRepository.save(mappedRequest);

        return TransactionMapper.Map(updateResult);
    }
}
