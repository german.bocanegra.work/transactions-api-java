package com.gbw.transactions_api.adapter.input.controllers;

import com.gbw.transactions_api.application.ports.input.transactions.IInitiateTransactionPort;
import com.gbw.transactions_api.application.ports.input.transactions.requests.CreateTransactionRequest;
import com.gbw.transactions_api.application.ports.input.transactions.validators.ICreateTransactionValidatorPort;
import com.gbw.transactions_api.domain.entities.Transaction;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SpringTransactionController {
    private final IInitiateTransactionPort initiateTransactionPort;
    private final ICreateTransactionValidatorPort createTransactionValidatorPort;

    public SpringTransactionController(IInitiateTransactionPort initiateTransactionPort, ICreateTransactionValidatorPort createTransactionValidatorPort) {
        this.initiateTransactionPort = initiateTransactionPort;
        this.createTransactionValidatorPort = createTransactionValidatorPort;
    }

    @RequestMapping(value="/transaction", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Transaction createTransaction(@RequestBody CreateTransactionRequest request){
        createTransactionValidatorPort.validateCreateTransactionRequest(request);
        return initiateTransactionPort.initiateTransaction(request);
    }
}
