package com.gbw.transactions_api.adapter.input.listeners;

import com.gbw.transactions_api.application.ports.input.transactions.IProcessTransactionPaymentPort;
import com.gbw.transactions_api.application.ports.input.transactions.IProcessTransactionWalletBalancePort;
import com.gbw.transactions_api.common.exceptions.RecoverableErrorException;
import com.gbw.transactions_api.domain.constants.TransactionEventsQueueNames;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.support.Acknowledgment;

import java.time.Duration;

@Configuration
public class SpringKafkaTransactionListeners {
    private final IProcessTransactionPaymentPort processTransactionPaymentPort;
    private final IProcessTransactionWalletBalancePort processTransactionWalletBalancePort;

    public SpringKafkaTransactionListeners(IProcessTransactionPaymentPort processTransactionPaymentPort, IProcessTransactionWalletBalancePort processTransactionWalletBalancePort) {
        this.processTransactionPaymentPort = processTransactionPaymentPort;
        this.processTransactionWalletBalancePort = processTransactionWalletBalancePort;
    }

    @Bean
    public NewTopic processPaymentTopic() {
        return TopicBuilder.name(TransactionEventsQueueNames.PROCESS_PAYMENT_TOPIC)
                .partitions(10)
                .replicas(1)
                .build();
    }

    @Bean
    public NewTopic processWalletBalanceTopic() {
        return TopicBuilder.name(TransactionEventsQueueNames.PROCESS_WALLET_BALANCE_TOPIC)
                .partitions(10)
                .replicas(1)
                .build();
    }

    @KafkaListener(id = TransactionEventsQueueNames.PROCESS_PAYMENT_TOPIC, topics = TransactionEventsQueueNames.PROCESS_PAYMENT_TOPIC)
    public void processPaymentListener(String in, Acknowledgment ack) {
        try {
            processTransactionPaymentPort.processPayment(in);
            ack.acknowledge();
        }
        catch (RecoverableErrorException e) {
            ack.nack(Duration.ofSeconds(10));
        }
        catch (Exception e) {
            ack.nack(Duration.ofMinutes(15));
        }
    }

    @KafkaListener(id = TransactionEventsQueueNames.PROCESS_WALLET_BALANCE_TOPIC, topics = TransactionEventsQueueNames.PROCESS_WALLET_BALANCE_TOPIC)
    public void walletBalanceListener(String in, Acknowledgment ack) {
        try {
            processTransactionWalletBalancePort.processWalletBalance(in);
            ack.acknowledge();
        }
        catch (RecoverableErrorException e) {
            ack.nack(Duration.ofSeconds(10));
        }
        catch (Exception e) {
            ack.nack(Duration.ofMinutes(15));
        }
    }
}
